let topLevel = 0;

function toTop(image) {
    const imageStyle = document.getElementById(image).style;
    imageStyle.zIndex = (++topLevel).toString();
}
