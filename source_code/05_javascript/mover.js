// mover.js
//   Illustrates moving an element within a document

// The event handler function to move an element
function moveIt(moveeID, newTop, newLeft) {

    movee = document.getElementById(moveeID);

// Change the top and left properties to perform the move
//  Note the addition of units to the input values 
    movee.style.top = newTop + "px";
    movee.style.left = newLeft + "px";
}
