const helpers = {
    "name": "Your name must be in the form:\nfirst name, middle initial., last name",
    "email": "Your email address must have the form: user@domain",
    "user": "Your user ID must have at least six characters",
    "password": "Your password must have at least six characters and it must include one digit",
    "default": "This box provides advice on filling out the form on this page. " +
        "Put the mouse cursor over any input field to get advice"
};


function messages(advice) {
    document.getElementById("adviceBox").value = helpers[advice];
}
