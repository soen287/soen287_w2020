function toggleImag() {
    const squirrel = document.getElementById("squirrel").style;

    if (squirrel.visibility === "hidden")
        squirrel.visibility = "visible";
    else
        squirrel.visibility = "hidden";
}
